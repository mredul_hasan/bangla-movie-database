<?php

// A list of permitted file extensions
$allowed = array('png', 'jpg', 'gif', 'jpeg');

if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){

	$extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);

	if(!in_array(strtolower($extension), $allowed)){
		echo '{"status":"error"}';
		exit;
	}

	if(move_uploaded_file($_FILES['upl']['tmp_name'], 'uploads/'.$_FILES['upl']['name'])){
		echo '{"status":"success"}';

		$path = 'uploads/'.$_FILES['upl']['name']);
		$last_id = mysqli_query($connection, "SELECT * FROM movie ORDER BY Movie_id DESC LIMIT 1");
		$insert_image = mysqli_query($connection, "UPDATE movie SET movie_poster = '" . $path . "' where Movie_id ='" . $last_id . "' " );
		exit;
	}
}

echo '{"status":"error"}';
exit;