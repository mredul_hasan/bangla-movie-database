-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2018 at 11:46 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `chitra_samachar`
--

-- --------------------------------------------------------

--
-- Table structure for table `actor`
--

CREATE TABLE IF NOT EXISTS `actor` (
  `Actor_id` int(11) NOT NULL AUTO_INCREMENT,
  `First_name` varchar(20) NOT NULL,
  `Last_name` varchar(20) NOT NULL,
  `birthday` date NOT NULL,
  `birth_place` varchar(50) NOT NULL,
  `Movie_id` int(11) NOT NULL,
  PRIMARY KEY (`Actor_id`),
  KEY `Movie_id` (`Movie_id`),
  KEY `Actor_id` (`Actor_id`),
  KEY `Movie_id_2` (`Movie_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `actor`
--

INSERT INTO `actor` (`Actor_id`, `First_name`, `Last_name`, `birthday`, `birth_place`, `Movie_id`) VALUES
(1, 'MD.', 'Razzak', '1945-08-02', 'borishal', 1),
(2, 'Alomgir', 'hossain', '1955-03-15', 'Rongpur', 2),
(3, 'Bulbul', 'Ahmed', '1940-06-26', 'Khulna', 6),
(4, 'Sakib', 'Khan', '1980-05-13', 'Noakhali', 10),
(5, 'MD.', 'Riaz', '1979-11-10', 'Dhaka', 8),
(6, 'Shakil ', 'Khan', '1981-08-16', 'Gaibandha', 9),
(7, 'Elias', 'Kanchan', '1970-08-16', 'Pabna', 12),
(8, 'Salman', 'Shah', '1971-03-15', 'Sylhet', 14),
(9, 'ferdaus', 'Raman', '1976-04-25', 'Dinajpur', 7),
(10, 'Shahin', 'Alam', '1990-04-28', 'Magura', 10),
(11, 'Omar', 'Sani', '1978-09-14', 'Jessore', 14);

-- --------------------------------------------------------

--
-- Table structure for table `actress`
--

CREATE TABLE IF NOT EXISTS `actress` (
  `Actress_id` int(11) NOT NULL,
  `First_name` varchar(20) NOT NULL,
  `Last_name` varchar(20) NOT NULL,
  `birthday` date NOT NULL,
  `birth_place` varchar(50) NOT NULL,
  `Movie_id` int(11) NOT NULL,
  PRIMARY KEY (`Actress_id`),
  KEY `Actress_id` (`Actress_id`),
  KEY `Movie_id` (`Movie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `actress`
--

INSERT INTO `actress` (`Actress_id`, `First_name`, `Last_name`, `birthday`, `birth_place`, `Movie_id`) VALUES
(0, 'Suchonda', 'Raihan', '1967-09-06', 'Sirajgonj', 9),
(1, 'Bobita', 'Rahman', '1955-12-31', 'Bandorban', 7),
(2, 'Kabori', 'Akhter', '1960-10-09', 'Bogura', 12),
(3, 'Shabana', 'Azmi', '1937-09-25', 'Norail', 8),
(4, 'Shabnur', 'khatun', '1978-08-16', 'Narayangonj', 9),
(5, 'Purnima', 'Chowdhury', '1975-08-22', 'Faridpur', 12),
(6, 'Apu', 'Bishawas', '1980-02-09', 'Netrokona', 14),
(7, 'Sreelekha', 'Mitra', '1987-09-06', 'Kolkata', 5),
(8, 'Mahiya', 'Mahui', '1989-09-11', 'Kishorgonj', 13),
(9, 'Ishita', 'Khandokar', '1980-09-07', 'Narail', 10);

-- --------------------------------------------------------

--
-- Table structure for table `director`
--

CREATE TABLE IF NOT EXISTS `director` (
  `Director_id` int(11) NOT NULL AUTO_INCREMENT,
  `First_name` varchar(20) NOT NULL,
  `Last_name` varchar(20) NOT NULL,
  `birthday` date NOT NULL,
  `birth_place` varchar(50) NOT NULL,
  `Gender` varchar(10) NOT NULL,
  `Movie_id` int(11) NOT NULL,
  PRIMARY KEY (`Director_id`),
  KEY `Movie_id` (`Movie_id`),
  KEY `Director_id` (`Director_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `director`
--

INSERT INTO `director` (`Director_id`, `First_name`, `Last_name`, `birthday`, `birth_place`, `Gender`, `Movie_id`) VALUES
(1, 'Johir ', 'Raihan', '1930-05-10', 'Narayongonj', 'male', 2),
(2, 'Chashi Nozrul', 'Islam', '1955-01-18', 'Bagerhat', 'male', 7),
(3, 'Suvash', 'Dutt', '1923-10-09', 'Kishoregonj', 'male', 7),
(4, 'Khan Ataur', 'Rahaman', '1954-10-13', 'Jessore', 'male', 6),
(5, 'Humayun', 'Ahmed', '1955-08-19', 'rongpur', 'male', 2),
(6, 'Kazi', 'Hayat', '1935-08-09', 'Dhaka', 'male', 10),
(7, 'Tareq', 'Masud', '1935-02-12', 'Madaripur', 'male', 8),
(8, 'Toukir', 'Ahmed', '1965-12-12', 'Mymensingh', 'male', 4),
(9, 'Nur', 'Islam', '1998-09-07', 'Gaibandha', 'male', 10),
(10, 'Bobita', 'Rahman', '1978-09-06', 'Gazipur', 'female', 13),
(11, 'Schorita', 'saha', '1967-09-15', 'khulna', 'female', 7),
(12, 'Anuradha', 'Ghoshal', '1987-09-06', 'kolkata', 'female', 11);

-- --------------------------------------------------------

--
-- Table structure for table `movie`
--

CREATE TABLE IF NOT EXISTS `movie` (
  `Movie_id` int(11) NOT NULL AUTO_INCREMENT,
  `Movie_name` varchar(50) NOT NULL,
  `Year` int(11) NOT NULL,
  `Genre` varchar(20) NOT NULL,
  `Rating` float NOT NULL,
  `movie_poster` varchar(100) NOT NULL,
  `description` text,
  `studio_id` int(11) NOT NULL,
  PRIMARY KEY (`Movie_id`),
  UNIQUE KEY `Movie_id` (`Movie_id`),
  KEY `st` (`studio_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `movie`
--

INSERT INTO `movie` (`Movie_id`, `Movie_name`, `Year`, `Genre`, `Rating`, `movie_poster`, `description`, `studio_id`) VALUES
(1, 'Mukh O Mukhos', 1923, 'Nirbak', 9, 'dummy/movie poster/slide-1.JPG', NULL, 2),
(2, 'amanush', 1995, 'comedy', 9.5, '', NULL, 7),
(3, 'devdas', 1996, 'romantic', 9.7, '', NULL, 4),
(4, 'hazar bochor dhore', 2005, 'village life', 9.4, '', NULL, 2),
(5, 'ora egarjon', 1973, 'freedom war', 9.9, '', NULL, 1),
(6, 'Jibon theke neoa', 1970, 'philosophical', 9.3, '', NULL, 2),
(7, 'padma nodir majhi', 1993, 'philosophical', 9.1, '', NULL, 3),
(8, 'simana periye', 1977, 'adventure', 9.2, '', NULL, 10),
(9, 'Chutir Ghonta', 1980, 'true story', 8.7, 'dummy/movie poster/slide-3.JPG', NULL, 8),
(10, 'dipu number 2', 1996, 'mystery', 0, 'dummy/movie poster/slide-2.JPG', NULL, 6),
(11, 'sareng bou', 1978, 'Philosophical', 8.5, '', NULL, 3),
(12, 'Rongbaz', 1973, 'Romantic', 9.2, '', NULL, 1),
(13, 'Titas ekti nodir nam', 1973, 'philosophical', 8.3, '', NULL, 7),
(14, 'Chitra nodir pare', 1999, 'historical', 9.45, '', NULL, 7),
(15, 'aguner proshmoni', 1995, 'freedom war', 9.8, '', NULL, 9),
(16, 'Shongkhochil', 2016, 'unknown', 0, 'path', NULL, 5),
(17, 'aynabaji', 2016, '', 0, '', NULL, 0),
(18, 'Aynabaji', 2016, '', 0, '', 'superb movie :)', 0),
(19, 'Aynabaji', 2016, 'staff', 0, '', 'superb movie :)', 0),
(20, 'aynabaji', 2016, 'staff', 0, '', 'superb movie :)', 0),
(21, 'aynabaji', 2016, 'Drama', 0, '', 'superb movie :)', 0),
(22, 'Aynabaji', 2016, 'War', 0, '', 'fantabulous :)\r\nmind blowing story line.', 0),
(23, '', 0, 'staff', 0, '', '', 0),
(24, '', 0, 'staff', 0, '', '', 0),
(25, '', 0, 'staff', 0, '', '', 0),
(26, '', 0, 'staff', 0, '', '', 0),
(27, '', 0, 'staff', 0, '', '', 0),
(28, '', 0, 'staff', 0, '', '', 0),
(29, 'Aynabaji', 2016, 'Romantic', 0, '', 'nice.', 0),
(30, 'Aynabaji', 2016, 'Romantic', 0, '', 'nice!`', 0),
(31, 'Aynabaji', 2016, 'Romantic', 0, '', 'nice!`', 0),
(32, 'aynabaji', 2016, 'Tragedy', 0, '', 'nice!', 0),
(33, '', 0, 'staff', 0, '', '', 0),
(34, '', 0, 'staff', 0, '', '', 0),
(35, '', 0, 'Tragedy', 0, '', 'nice!', 0),
(36, '', 0, 'Tragedy', 0, '', 'nice!', 0),
(37, '', 0, 'staff', 0, '', '', 0),
(38, '', 0, 'staff', 0, '', '', 0),
(39, '', 0, 'staff', 0, '', '', 0),
(40, '', 0, 'staff', 0, '', '', 0),
(41, '', 0, 'staff', 0, '', '', 0),
(42, '', 0, 'staff', 0, '', '', 0),
(43, '', 0, 'staff', 0, '', '', 0),
(44, '', 0, 'staff', 0, '', '', 0),
(45, '', 0, 'staff', 0, '', '', 0),
(46, '', 0, 'staff', 0, '', '', 0),
(47, '', 0, 'staff', 0, '', '', 0),
(48, '', 0, 'staff', 0, '', '', 0),
(49, '', 0, 'staff', 0, '', '', 0),
(50, '', 0, 'staff', 0, '', '', 0),
(51, '', 0, 'staff', 0, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE IF NOT EXISTS `rate` (
  `Rating` float NOT NULL,
  `Comment` text NOT NULL,
  `User_id` int(11) NOT NULL,
  `Movie_id` int(11) NOT NULL,
  PRIMARY KEY (`User_id`,`Movie_id`),
  KEY `User_id` (`User_id`),
  KEY `Movie_id` (`Movie_id`),
  KEY `Movie_id_2` (`Movie_id`),
  KEY `User_id_2` (`User_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`Rating`, `Comment`, `User_id`, `Movie_id`) VALUES
(9, 'My favourite one.', 1, 1),
(9, 'awesome', 3, 2),
(10, 'onek valo', 3, 10),
(8, 'valoi legeche', 4, 11),
(10, 'bar bar dekhte chai', 5, 6),
(10, 'the best one', 6, 9),
(10, 'amar jioboner shreshto cinema', 6, 12),
(8, 'awesome story', 7, 14),
(9, 'valo cinema gulor moddhe ekta', 8, 12),
(8, 'cinemar kahini onek sundor', 15, 5);

-- --------------------------------------------------------

--
-- Table structure for table `studio`
--

CREATE TABLE IF NOT EXISTS `studio` (
  `studio_id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(20) NOT NULL,
  `City` varchar(20) NOT NULL,
  PRIMARY KEY (`studio_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `studio`
--

INSERT INTO `studio` (`studio_id`, `Name`, `City`) VALUES
(1, 'Fox', 'Newyork'),
(2, 'Disney', 'London'),
(3, 'Pixur', 'Los Angeles'),
(4, 'Eskay', 'Kolkata'),
(5, 'Raj Laxmi', 'Dhaka'),
(6, 'Venktesh Film', 'TallyGonj'),
(7, 'Raj Production', 'Baily Road'),
(8, 'Fantasy Production', 'Narayangonj'),
(9, 'Gold Star', 'Beijing'),
(10, 'Yash Raj Films', 'Mumbai');

-- --------------------------------------------------------

--
-- Table structure for table `upcoming`
--

CREATE TABLE IF NOT EXISTS `upcoming` (
  `Movie` int(11) NOT NULL,
  `Release_date` date NOT NULL,
  PRIMARY KEY (`Movie`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `User_id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_type` varchar(30) NOT NULL,
  PRIMARY KEY (`User_id`),
  UNIQUE KEY `email` (`email`),
  KEY `id` (`User_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`User_id`, `name`, `email`, `password`, `last_login`, `user_type`) VALUES
(1, 'mredul', 'mredulhasan93@gmail.com', '62a5575cc3f63d9d98e085a395e60599', '2018-08-30 19:49:56', 'Admin'),
(9, 'sumit', 'sumit.ku.cse@gmail.com', 'ee8f75d19d50b8c8557e3661fe66e2bf', '2016-11-08 23:26:04', 'Admin'),
(3, 'farhana', 'farhana@gmail.com', 'f6c29ea92aab180183100dff95cce70a', '2016-10-25 21:11:34', 'Admin'),
(4, 'akash', 'akash@gmail.com', '4c701c72ba7fd76047164cea2ea6df39', '2016-10-25 20:59:09', ''),
(5, 'deep', 'deep@gmail.com', 'ae18daae09297b2a35b0be4294e7fb09', '2016-10-25 20:59:09', ''),
(6, 'soma', 'soma@gmail.com', '47f705c97fbda8775beae1850f8f3f33', '2016-10-25 20:59:09', ''),
(7, 'juel', 'juel@gmail.com', '4ca0cc8c9e69cde047b0339ec2fa971b', '2016-10-25 20:59:09', ''),
(8, 'nazia', 'nazia@gmail.com', 'da4bc744ceb0c2276d10ba96326aa270', '2016-10-25 20:59:09', ''),
(10, 'fazla', 'fazla@gmail.com', '556485260fcf2fdf131c8a931126a7ff', '2016-10-25 20:59:09', ''),
(11, 'nadira', 'nadira@gmail.com', 'a7a755458b2bbda537a10905b6e8808a', '2016-10-25 20:59:09', ''),
(12, 'avijit', 'avijit@gmail1.com', 'cb7e427a99be564cac6f045de0721894', '2016-10-25 20:59:09', ''),
(13, 'rahad', 'rahad@gmail.com', 'f6b006b13947a48346270bc3dd3c7c8b', '2016-10-25 23:04:53', 'General'),
(14, 'riya', 'riya@gmail.com', '19e960a3d174958090c530666d4aa507', '2016-10-25 20:59:09', ''),
(15, 'mishuk', 'mishuk@gmail.com', 'f474dff115cd4be8dad153b0d5702ad2', '2016-10-25 20:59:09', ''),
(16, 'fahim', 'fahim@gmail.com', '78946e1aab6abcf907f5afeb833bfdea', '2016-10-25 20:59:09', ''),
(17, 'arpon', 'arpon@gmail.com', '3c5dcc66436ebf0b2211d90417222d6c', '2016-10-25 20:59:09', ''),
(19, 'saidur', 'saidur@gmail.com', 'e21c4d7aeed37ddd7597fdc22b59f77d', '2016-10-25 20:59:09', ''),
(21, 'kushal', 'kushal@gmail.com', '4cddb1026ee310a1c6f7c256853ccc11', '2016-10-25 20:59:09', ''),
(22, 'nionta', 'nionta@gmail.com', 'c8bbe2bbb59e238acf6f665eb11eba59', '2016-10-25 20:59:09', ''),
(23, 'pappu', 'pappu@gmail.com', 'd93f5cee6a56f17fa00c0a5c974988a0', '2016-10-25 20:59:09', ''),
(24, 'tajul', 'tajul@gmail.com', 'ace8db6ec0f74060c49f90acedd0fa57', '2016-10-25 20:59:09', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
